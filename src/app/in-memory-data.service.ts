import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';
import { Injectable } from '@angular/core';

@Injectable({
providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
createDb() {
const heroes = [
{ id: 1, name: 'Aaaaa', marks: 95 },
{ id: 2, name: 'Bbbbb', marks: 70},
{ id: 3, name: 'Ccccc', marks: 80 },
{ id: 4, name: 'Ddddd', marks: 60},
{ id: 5, name: 'Eeeee', marks: 50},
{ id: 6, name: 'Fffff', marks: 55},
{ id: 7, name: 'Ggggg', marks: 65},
{ id: 8, name: 'Hhhhh', marks: 75},
{ id: 9, name: 'Iiiii', marks: 85},
{ id: 10, name: 'Jjjjj', marks: 90 }
];
return {heroes};
}

// Overrides the genId method to ensure that a hero always has an id.
// If the heroes array is empty,
// the method below returns the initial number (11).
// if the heroes array is not empty, the method below returns the highest
// hero id + 1.
genId(heroes: Hero[]): number {
return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 1;
}
}